$(document).ready(function() {
    $("#name").click(function() {
        $('#search').change(function () {
            $("#search").attr("placeholder", "Search a Car...");
        });
    })

    $("#price").click(function() {
        $('#search').change(function () {
            $("#search").attr("placeholder", "Search by Car Price per day...");
       
        });
    })

    $('#search').on("keyup", function() {  
        $.ajax({
            url: `/carSearch/`,
            data: { "search": $(this).val().toLowerCase()},
            dataType: 'json',
            success: function (data) {
                $(".carDisplay").html(data.map(car =>
                    `<div class="card">
                        <div class="card-body text-center">
                            <img src="https://pbs.twimg.com/media/EXpGD2hVcAEnABZ?format=png&name=small" class="pict">
                            <h5 class="card-title">${car.car_name }</h5>
                            <h6 class="card-subtitle mb-2 text-muted" style="margin-bottom: 4px;">${car.city}</h6>
                            <p class="card-text" style="margin-bottom: 8px;">Rp. ${car.price},00 / day</p>
                            <input type='hidden' name='car_name' value='${car.car_name}'>
                            <input type='hidden' name='user_name' value='${car.user_name}'>
                            <input type="submit" class="button btnClass" value='View Details'>
                        </div>
                    </div>`
                ))
            }
        })    
    });
});

// $(document).ready(function() {
//     $('#search').on("keyup", (function() {
//         $.ajax({
//             url: `/carSearch/`,
//             data: { "search": $(this).val().toLowerCase()},
//             dataType: 'json',
//             success: function(data) {
//                 $('.carDisplay').html('')
//                 var result = '';
//                 for (var i = 0; i < data.length; i++) {
//                         result += `<div class="card-body text-center">`;
//                         result += `<img src="{% static 'images/car.png' %}" class="pict">`;
//                         result += `<h5 class="card-title">`;
//                         result += `${car.car_name}`;
//                         // result += data[i].car_name;
//                         result += `</h5>`;
//                         result += `<h6 class="card-subtitle mb-2 text-muted" style="margin-bottom: 4px;">`;
//                         result += `${car.city}`;
//                         // result += data[i].city;
//                         result += `</h6>`;
//                         result += `<p class="card-text" style="margin-bottom: 8px;">Rp. `;
//                         result += `${car.price}`;
//                         // result += data[i].price;
//                         result += `,00 / day</p>`;
//                         result += `<input type="submit" class="button btnClass" value='View Details' action="/viewCar/`;
//                         result += `${car.id}`;
//                         // result += data[i].id;
//                         result += `/>`;
//                         index++;
//                 }
//                 $('.carDisplay').append(result);
//             },
//             error: function(error) {
//                 alert("NO CAR IS FOUND");
//             }
//         })
//     }));
// });
