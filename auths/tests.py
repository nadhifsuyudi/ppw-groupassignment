from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.apps import apps
from .apps import *
from .views import *
from django.contrib.auth.forms import User


class UnitTest(TestCase):
    def test_login_app(self):
        self.assertEqual(AuthsConfig.name, 'auths')
        self.assertEqual(apps.get_app_config(
            'auths').name, 'auths')

    
    def testRegisterURL(self):
        response = Client().get('/auths/register/')
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 404)

    def testRegisterFunction(self):
        found = reverse('auths:register')
        self.assertEqual(resolve(found).func, register)


    def testAuthAppName(self):
        self.assertEqual(apps.get_app_config(
            'auths').name, 'auths')

    def testLoginURL(self):
        response = Client().get('/auths/login/')
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(response.status_code, 404)

    def testRegisterTemplate(self):
        response = Client().get('/auths/register/')
        self.assertTemplateUsed(response, 'register.html')

    def testLoginTemplate(self):
        response = Client().get('/auths/login/')
        self.assertTemplateUsed(response, 'registration/login.html')
        self.assertContains(response, 'login')
        
    def testRegisterform(self):
        response = self.client.post(reverse('auths:register'), data={'username':'qweqweewqewq', 'email':'sangatrandom123@gmail.com', 'first_name':'sangat', 'last_name':'random', 'password1':'qweqwe123321', 'password2':'qweqwe123321'})
        count = User.objects.all().count()
        input = User.objects.all().first()
        self.assertEqual(input.username, 'qweqweewqewq')
        self.assertEqual(count, 1)
        self.assertNotEqual(count, 0)