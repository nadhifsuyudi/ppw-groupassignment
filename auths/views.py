
from django.shortcuts import render, redirect
from auths.forms import RegistrationForm
#from django.contrib.auth import authenticate, login


def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('auths:login')
    else:
        form = RegistrationForm()
    return render(request, 'register.html', {'form': form})

'''
def authentication_login(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    
    if user is not None:
        request.session['user_login'] = username
        messages.success(request, "Login successful")
        print(username)
        res = HttpResponseRedirect(reverse('car_rental:index'))

        res.set_cookie('user_name', username)
        return res
    else:
        messages.error(request, "Account not found, please check your credentials")
        return HttpResponseRedirect(reverse('car_rental:index'))


def authentication_logout(request):
    request.session.flush()
    res = HttpResponseRedirect('/auths/login')
    res.delete_cookie('user_name')

    messages.info(request, "Logout successful, current session is successfully deleted")
    return HttpResponseRedirect(reverse('car_rental:login'))
    '''