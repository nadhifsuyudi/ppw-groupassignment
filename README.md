### Group Members
- 1806173613 Azzahra Abraara
- 1806241236 Wilson Hadi Junior
- 1806241021 Aljihad Ijlal Nadhif Suyudi
- 1806241160 Priagung Satria Wicaksana

### Link to HerokuApp
Link: http://ppw-ga.herokuapp.com/

### Description
The application will be a website created for people interested to rent or lease their car. 

Users will be able to search for cars they are interested in renting and a list of cars will show up based on what they search for. For example, searching 'honda' or '2014' will return a list of cars that contain those keywords. When the user clicks on a car they are interested in, they will be able to see the specifications (including price, city, year of making, etc), read reviews of past renters, view transactions that has been made for that car and click a button to rent it. 

When users want to rent a car, they click the 'rent' button. Then they will be redirected to a page where they will fill out a form to make a transaction and rent that car. Then that transaction will be added to the database.

Users may also add their own cars to then be available for other people to rent. They will fill out a form to fill out the details of their car and after submitting, their car will be added to the database.

The application will also have an article page where users are able to view articles about recommended destinations to visit as a way of possibly increasing the number of people renting the cars available in our app.

### Features
- Cars (add, search and view)
- Transactions (make and view transactions)
- Review (view reviews for each car)
- Article (view articles about tourist destinations)

### Link to Wireframe and High Fidelity Mockup
(both wireframe and mockup is created in figma)
Link: https://www.figma.com/file/6qGPb2BMlU8OxTmu2Vf6J2/Untitled?node-id=0%3A1
