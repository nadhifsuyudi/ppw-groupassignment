from django.urls import path
from . import views

app_name = 'car_rental'

urlpatterns = [
    path('', views.index, name='index'),
    path('car/', views.car, name='car'),
    path('addCar/', views.addCar, name='addCar'),
    path('transaction/', views.transaction, name='transaction'),
    path('article/', views.article, name='article'),
    path('addArticle/', views.addArticle, name='addArticle'),
    path('articleView/', views.articleView, name='articleView'),
    path('carSearchHomepage/', views.carSearchHomepage, name='carSearchHomepage'),
    path('update_rating/', views.update_rating, name='update_rating'),
    path('carSearch/', views.carSearch, name='carSearch'),
    path('viewCar/', views.viewCar, name='viewCar'),
    path('rentCar/', views.rentCar, name='rentCar'),
    path('favoriteCar/', views.favoriteCar, name='favoriteCar'),
    path('unfavoriteCar/', views.unfavoriteCar, name='unfavoriteCar'),
]