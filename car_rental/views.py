from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt

from .models import *
from . import forms
from .forms import AddCarForm, RentCarForm, AddArticleForm
import re
import json

counter = 0
id = 999999

# Create your views here.
def index(request):
    car = Car.objects.all()
    response = {'car' : car}
    return render(request, 'index.html', response)

def car(request):
    car = Car.objects.all()
    response = {'car' : car}
    return render(request, 'car.html', response)

def carSearchHomepage(request):
    search = request.POST['search']
    car = Car.objects.filter(car_name__icontains=search)
    if not car.exists():
        car = Car.objects.filter(price__range=(0,search))
    response = {'car' : car}
    return render(request, 'car.html', response)

def carSearch(request):
    search = request.POST['search']
    carTarget = Car.objects.filter(car_name__icontains=search)
    if not carTarget.exists():
        carTarget = Car.objects.filter(price__range=(0,search))
    response = []
    for car in carTarget:
        tempvar = {'car_name' : car.car_name, 'user_name' : car.user_name,  'city' : car.city,  'price' : car.price,'id' : car.id}
        response.append(tempvar)
    return JsonResponse(response, safe=False)

def viewCar(request):
    target_name = request.POST['car_name']
    target_user = request.POST['user_name']
    car = Car.objects.get(car_name=target_name, user_name=target_user)
    response = {'car' : car}
    return render(request, 'viewCar.html', response)

def addCar(request):
    if request.method == 'POST':
        form = forms.AddCarForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('car_rental:car')
    else:
        form = forms.AddCarForm()
        return render(request, 'addCar.html', {'form': form})

def rentCar(request):
    transaction = Transaction.objects.order_by('-date')
    if request.method == 'POST':
        form = forms.RentCarForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('../transaction')
    else:
        form = forms.RentCarForm()
    response = {'transaction': transaction, 'form': form, 'car': car}
    return render(request, 'rentCar.html', response)

def transaction(request):
    car = Car.objects.all()
    transaction = Transaction.objects.all().order_by('-date')
    response = {'car' : car, 'transaction': transaction}
    return render(request, 'transaction.html', response)

def article(request):
    artic = Article.objects.all()
    response = {'article': artic}
    return render(request, 'article.html', response)

def addArticle(request):
    if request.method == 'POST':
        form = forms.AddArticleForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('car_rental:article')
    else:
        form = forms.AddArticleForm()
        return render(request, 'addArticle.html', {'form': form})

def articleView(request):
    arti_title = request.POST['title']
    articles = Article.objects.all()
    for article in articles:
        if arti_title == article.title:
            articleTarget = article
    response = {'article' : articleTarget}

    return render(request, 'articleView.html', response)

def update_rating(request):
    if request.method == 'GET':
        print('im here')
        print("REQUEST :",request.GET)
        titlestring = request.GET['title']

        article = Article.objects.get(title=titlestring) #getting the liked posts
        article.total_like += 1
        article.save()  # saving it to store in database
        return HttpResponse(article.total_like) # Sending an success response
    return HttpResponse("Request method is not a GET")


def favoriteCar(request):
    target_person = request.user
    if request.method == 'POST':
        target_user = request.POST['user_name']
        target_name = request.POST['car_name']
        target = Car.objects.get(car_name=target_name, user_name=target_user)
        if not FavoriteCar.objects.filter(car=target, person=target_person).exists():
            new = FavoriteCar()
            new.car = target
            new.person = target_person
            new.user = target_user
            new.save()
    favCars = FavoriteCar.objects.filter(person=target_person)
    carList = favCars.values_list('car')
    car = Car.objects.filter(id__in=carList)
    response = {'car' : car}
    return render(request, 'favoriteCar.html', response)
    
def unfavoriteCar(request):
    target_person = request.user
    if request.method == 'POST':
        target_user = request.POST['username']
        target_name = request.POST['carname']
        target_car = Car.objects.get(car_name=target_name, user_name=target_user)
        target = FavoriteCar.objects.get(person=target_person, car=target_car)
        target.delete()
    favCars = FavoriteCar.objects.filter(person=target_person)
    carList = favCars.values_list('car')
    car = Car.objects.filter(id__in=carList)
    response = {'car' : car}
    return render(request, 'favoriteCar.html', response)