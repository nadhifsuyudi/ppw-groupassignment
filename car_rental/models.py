from django.db import models
from django.forms import ModelForm
from django.contrib.auth.models import User

# Create your models here.

class Car(models.Model):
    car_name = models.CharField(max_length=30)
    user_name = models.CharField(max_length=30)
    year = models.CharField(max_length=4)
    city = models.CharField(max_length=30)
    price = models.CharField(max_length=30)
    description = models.TextField()

    def __str__(self):
        return self.car_name

class FavoriteCar(models.Model):
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    person = models.ForeignKey(User, on_delete = models.CASCADE, primary_key = False)
    user = models.CharField(max_length=30)

class Transaction(models.Model):
    user_name = models.CharField(max_length=30)
    car = models.ForeignKey(Car, on_delete=models.CASCADE)
    start_date = models.DateField()
    end_date = models.DateField()
    date = models.DateField(auto_now_add=True)

class Article(models.Model):
    title = models.CharField(max_length=30)
    date = models.DateField()
    short_desc = models.TextField()
    content = models.TextField()
    photo = models.ImageField(upload_to='images/', default= 'images/test.jpg')
    total_like = models.IntegerField(default=0)

    def __str__(self):
        return self.title