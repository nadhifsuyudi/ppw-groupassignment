from django.test import TestCase, Client, SimpleTestCase
from django.http import HttpRequest
from django.urls import reverse, resolve
from . import views, models
from .views import *
from .models import *
from django.contrib.auth.models import User


class HomePageTests(SimpleTestCase):

    def test_url_index_exist(self):
        response= self.client.get('')
        self.assertEqual(response.status_code,200)

    def test_home_page_does_not_contain_incorrect_html(self):
        response = self.client.get('')
        self.assertNotContains(response, 'Whoops Wrong page.')

class RentalTests(TestCase):

    def test_articel_exist(self):
        response= self.client.get('/article')
        self.assertEqual(response.status_code,301)

    def test_article(self):
            artic = Article.objects.create(title='hehehe', date='2020-03-18', short_desc='ehfa', content='leol', photo='gaor.png')
            self.assertTrue(artic.__str__(), artic.title)
            self.assertTrue(isinstance(artic, Article))
            count_artic = Article.objects.all().count()
            self.assertEqual(count_artic, 1)

    def test_home_page_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')

    def test_car_page_url_is_exist(self):
        response = Client().get('/car/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'car.html')

    def test_addCar_page_url_is_exist(self):
        response = Client().get('/addCar/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'addCar.html')

    def test_transaction_page_url_is_exist(self):
        response = Client().get('/transaction/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'transaction.html')

    def test_article_page_url_is_exist(self):
        response = Client().get('/article/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'article.html')

    ####################### check function ########################

    def test_home_page_resolves(self):
            url = reverse('car_rental:index')
            self.assertEquals(resolve(url).func, index)

    def test_car_page_resolves(self):
        url = reverse('car_rental:car')
        self.assertEquals(resolve(url).func, car)

    def test_addCar_page_resolves(self):
        url = reverse('car_rental:addCar')
        self.assertEquals(resolve(url).func, addCar)

    def test_viewCar_page_resolves(self):
        url = reverse('car_rental:index')

class TestGA2Zahra(TestCase):

    def test_car_response_function_template(self):
        found = resolve('/car/')
        response = Client().get('/car/')
        self.assertEqual(found.func, car)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'car.html')

    def test_create_car(self):
        newCar = Car.objects.create(car_name = 'test', user_name = 'test', year = '2000', city = 'test', price = '100000', description = 'test')
        count = Car.objects.all().count()
        self.assertEqual(count, 1)

    def test_rentCar_page_resolves(self):
        url = reverse('car_rental:rentCar')
        self.assertEquals(resolve(url).func, rentCar)

    def test_viewCar_page_resolves(self):
        url = reverse('car_rental:viewCar')
        self.assertEquals(resolve(url).func, viewCar)

    def test_addCar_form(self):
        response = self.client.post('/addCar/', data={'car_name' : 'test', 'user_name' : 'testUser', 'city' : 'jkt', 'year' : '2000', 'price' : '20000', 'description' : 'test'})
        count = Car.objects.all().count()
        self.assertEqual(count, 1)
        self.assertEqual(response.status_code, 302)

    def test_rentCar_form(self):
        newCar = Car.objects.create(car_name = 'test', user_name = 'test', year = '2000', city = 'test', price = '100000', description = 'test')
        response = self.client.post('/rentCar/', data={'user' : 'test', 'car_name' : 'test', 'start_date' : '2020-01-01', 'end_date' : '2020-01-02'})
        count = Car.objects.all().count()
        self.assertEqual(count, 1)
        self.assertEqual(response.status_code, 200)


class TestGA2Wilson(TestCase):

    def test_favoriteCar_response_function_template(self):
        user = User.objects.create(username="test")
        user.set_password('p1p2p3p4')
        user.save()
        client = Client()
        client.login(username='test', password='p1p2p3p4')
        found = resolve('/favoriteCar/')
        response = client.get('/favoriteCar/')
        self.assertEqual(found.func, favoriteCar)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'favoriteCar.html')

    def test_create_favoriteCar_with_login(self):
        user = User.objects.create(username="test")
        newCar = Car.objects.create(car_name = 'test', user_name = 'test', year = '2000', city = 'test', price = '100000', description = 'test')
        new = FavoriteCar.objects.create(car = newCar, person = user, user = 'test')
        count = FavoriteCar.objects.all().count()
        self.assertEqual(count, 1)

    def test_add_to_favorite_cars_with_login(self):
        user = User.objects.create(username="test")
        user.set_password('p1p2p3p4')
        user.save()
        client = Client()
        client.login(username='test', password='p1p2p3p4')
        newCar = Car.objects.create(car_name = 'test', user_name = 'test', year = '2000', city = 'test', price = '100000', description = 'test')
        response = client.post('/favoriteCar/', data={'car' : newCar, 'car_name' : 'test', 'user_name' : 'test'})
        count = FavoriteCar.objects.all().count()
        self.assertEqual(count, 1)

    def test_remove_from_favorite_cars_with_login(self):
        user = User.objects.create(username="test")
        user.set_password('p1p2p3p4')
        user.save()
        client = Client()
        client.login(username='test', password='p1p2p3p4')
        newCar = Car.objects.create(car_name = 'test', user_name = 'test', year = '2000', city = 'test', price = '100000', description = 'test')
        new = FavoriteCar.objects.create(car = newCar, person = user, user = 'test')
        response = client.post('/unfavoriteCar/', data={'username' : 'test', 'carname' : 'test'})
        count = FavoriteCar.objects.all().count()
        self.assertEqual(count, 0)

    def test_searchCarHomepage(self):
        newCar = Car.objects.create(car_name = 'test', user_name = 'test', year = '2000', city = 'test', price = '10000', description = 'test')
        response = self.client.post('/carSearchHomepage/', data={'search' : 'test'})
        self.assertEqual(response.status_code, 200)
        response = self.client.post('/carSearchHomepage/', data={'search' : '10000'})
        self.assertEqual(response.status_code, 200)

    def test_searchCar(self):
        newCar = Car.objects.create(car_name = 'test', user_name = 'test', year = '2000', city = 'test', price = '10000', description = 'test')
        response = self.client.post('/carSearch/', data={'search' : 'test'})
        self.assertEqual(response.status_code, 200)
        response = self.client.post('/carSearch/', data={'search' : '10000'})
        self.assertEqual(response.status_code, 200)

    def test_viewCar(self):
        newCar = Car.objects.create(car_name = 'test', user_name = 'test', year = '2000', city = 'test', price = '10000', description = 'test')
        response = self.client.post('/viewCar/', data={'car_name' : 'test', 'user_name' : 'test'})
        self.assertTemplateUsed(response, 'viewCar.html')

class TestArticle(TestCase):

    def test_article_page_url_is_exist(self):
        response = Client().get('/article/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'article.html')

    def test_addArticle_page_url_is_exist(self):
        response = Client().get('/addArticle/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'addArticle.html')

    def test_article_page_resolves(self):
        url = reverse('car_rental:article')
        self.assertEquals(resolve(url).func, article)

    def test_addArticle_page_resolves(self):
        url = reverse('car_rental:addArticle')
        self.assertEquals(resolve(url).func, addArticle)

    def test_addArticle_with_login(self):
        user = User.objects.create(username="test")
        user.set_password('p1p2p3p4')
        user.save()
        client = Client()
        client.login(username='test', password='p1p2p3p4')
        newArticle = Article.objects.create(title='Article1', date='2020-03-18', short_desc='Short desc 1', content='Content 1', photo='article1.png')
        self.assertTrue(isinstance(newArticle, Article))
        count_artic = Article.objects.all().count()
        self.assertEqual(count_artic, 1)

    def test_likeArticle_with_login(self):
        user = User.objects.create(username="test")
        user.set_password('p1p2p3p4')
        user.save()
        client = Client()
        client.login(username='test', password='p1p2p3p4')
        newArticle = Article.objects.create(title='Article1', date='2020-03-18', short_desc='Short desc 1', content='Content 1', photo='article1.png', total_like='2')
        self.assertTrue(isinstance(newArticle, Article))
        self.assertEqual(newArticle.total_like, '2')

    def test_addArticle_form(self):
        response = self.client.post('/addArticle/', data={'title':'Article1', 'date':'2020-03-18', 'short_desc':'Short desc 1', 'content':'Content 1', 'photo':'article1.png'})
        count = Article.objects.all().count()
        self.assertEqual(count, 1)
        self.assertEqual(response.status_code, 302)

    def test_articleView_page_resolves(self):
        newArticle = Article.objects.create(title='Article1', date='2020-03-18', short_desc='Short desc 1', content='Content 1', photo='article1.png')
        response = self.client.post('/articleView/', data={'title':'Article1'})
        self.assertTemplateUsed(response, 'articleView.html')