# Generated by Django 3.0.6 on 2020-05-15 07:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('car_rental', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article',
            name='short_desc',
            field=models.TextField(),
        ),
    ]
